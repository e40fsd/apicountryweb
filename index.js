/* js for api */

document.addEventListener('DOMContentLoaded', () => {
    const countriesContainer = document.getElementById('countries-container');

    fetch('https://restcountries.com/v3.1/all')
        .then(response => response.json())
        .then(data => {
            data.forEach(country => {
                const countryCard = document.createElement('article');
                
                const countryName = document.createElement('h2');
                countryName.textContent = country.name.common;
                
                const countryFlag = document.createElement('img');
                countryFlag.src = country.flags.png;
                countryFlag.alt = `Flag of ${country.name.common}`;
                
                countryCard.appendChild(countryFlag);
                countryCard.appendChild(countryName);
                
                countriesContainer.appendChild(countryCard);
            });
        })
        .catch(error => console.error('Error fetching countries:', error));
});
document.getElementById('infoForm').addEventListener('submit', function(event) {
    event.preventDefault();
    
    const name = document.getElementById('name').value.trim();
    const email = document.getElementById('email').value.trim();
    const message = document.getElementById('message').value.trim();

    if (name && email && message) {
        alert('Form submitted successfully!');
    } else {
        alert('Please fill in all fields.');
    }
});